#!/bin/bash
#This script will create a question bank from the provided file

#Check if the file name is provided as argument
if [ -z "answer_file.csv" ]
then
    #If an argument is not provided, look for question_bank.txt
    file="question_bank.txt"
else
    #Else use the provided argument as the file name
    file="answer_file.csv"
fi

#Check if the file exists
if [ -f "$file" ]
then
    #If file exists, start creating the question bank
    echo "Creating question bank from $file"
    #Loop through the file line by line
    while read line
    do
        #Split the line into question and answer
        question=$(echo $line | awk -F "|" '{print $1}')
        answer=$(echo $line | awk -F "|" '{print $2}')
        #Print the question and answer, then prompt user for the next input
        echo $question
        read user_answer
        #Check if the answer is correct
        if [ "$user_answer" == "$
