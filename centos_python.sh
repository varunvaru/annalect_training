#!/bin/bash
echo "running new python dockerfile"
docker build -t dockerfile .
echo "running the container"
docker run -dt --name mypython dockerfile
docker commit mypython pynew_img
docker run -dt --name python_container2 pynew_img
docker exec -it python_container2 dd if=/dev/zero of=/root/python_file.txt bs=500M count=1
docker commit python_container2 pynew_img1
docker run -dt pynew_img1 sh
touch history_file_python.txt
docker history pynew_img1 >> history_file_python.txt


echo "running new nginx dockerfile"
docker build -t dockerfile annalect_training/
echo "running the container"
docker run -dt --name mynginx dockerfile
docker commit mynginx nginxnew_img
docker run -dt --name nginx_container2 nginxnew_img
docker exec -it nginx_container2 dd if=/dev/zero of=/root/python_file.txt bs=500M count=1
docker commit nginx_container2 nginxnew_img1
docker run -dt nginxnew_img1 sh
touch history_file_nginx.txt
docker history nginxnew_img1 >> history_file_nginx.txt
