#!/bin/bash


for ((i=1; i<=10; i++))
do
   docker run -dt --name python_container_$i python:latest

   echo "Container $i created with ID: $(docker ps -q -f name=python_container_$i)"
done

echo "Getting all running containers..."
CONTAINERS=$(docker ps -q)

for CONTAINER in $CONTAINERS
do
    echo "Healthchecking ${CONTAINER}..."
    docker container inspect --format='{{json .State}}' $CONTAINERS
done
