#!/bin/bash


echo "Creating 6 containers"
for i in {1..6}
do
    docker run -dt --name container$i ubuntu
    echo "containers created"
done

echo -n "Please enter the network name: "
read network_name

echo "Creating a customised bridge network"
docker network create --driver bridge $network_name

echo "Connecting each container to the network '$network_name'..."
for i in {1..6}
do
    docker network connect $network_name container$i
done

read -p "Give new name to a container that you want to link:" b
docker run -dt --name $b --network $network_name --link container1:container2 --link container2:container3 --link container3:container4 --link container4:container5 --link container5:container6 --link container6:container1 busybox sh

echo "Getting all running containers..."
CONTAINERS=$(docker ps | grep container)
touch link_healthcheck.txt
for CONTAINER in $CONTAINERS
do
    echo "Healthchecking ${CONTAINER}..."
    docker container inspect --format='{{json .State}}' $CONTAINERS > link_healthcheck.txt
    echo "Health checking completed" 
done
