provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "myec2" {
  count         = 3
  ami           = "ami-0cca134ec43cf708f"
  instance_type = "t2.micro"
  key_name      = "my-key-pair"
  security_groups = ["sg-08a8e1f6177361d73"]
}

output "instance_ips" {
  value = aws_instance.example.*.public_ip
}
