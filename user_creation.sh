#!/bin/bash

# This script will create multiple users

# Ask for user name
read -p 'Enter username: ' username

# Ask for real name
read -p 'Enter real name: ' realname

# Ask for password
read -s -p 'Enter password: ' password

# Ask for number of users
read -p 'How many users do you want to create: ' number

# Generate the users
for ((n=1; n<=$number; n++))
do
	useradd -c "$realname" -m $username$n
  echo $password | passwd --stdin $username$n
  echo "User account '$username$n' created with password '$password'"
done
