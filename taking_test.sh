#!/bin/bash
TIMEOUT=10
echo "The Test starts now!"
echo "First Question"
echo "Time remaining 10s"
echo "Which one of these is a fruit?"
echo "a. Apple"
echo "b. Onion"
echo "c. Tomato"
echo "d. Cabbage"

read -t $TIMEOUT  -p "Which one of these is a fruit? [a-d] " CHOICE

echo "$CHOICE,$SECONDS" >> answer_file.csv

if [ "$CHOICE" == "a" ]; then
  echo "Correct!"
else
  echo "Incorrect!"
fi
echo "Second Question"
echo "Time remaining 10s"
echo "Which one of these is a vegetable?"
echo "a. Orange"
echo "b. Banana"
echo "c. Beans"
echo "d. Mango"


read -t $TIMEOUT -p "Which one of thses is a vegetable? [a-d] " CHOICES

echo "$CHOICES,$SECONDS" >> answer_file.csv

if [ "$CHOICES" == "c" ]; then
  echo "Correct!"
else
	echo "Incorrect!"
fi

exit

