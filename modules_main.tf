provider "aws" {
  region  = "ap-south-1"
}

resource "aws_instance" "my_ec2" {
 ami = "var.ami_id"
 instance_type = "var.ec2_instance_type"
 key_name = "aws_key"

user_data = <<-EOF
#/bin/sh!
sudo apt-get update
sudo apt-get install apache2
sudo systemctl status apache2
sudo systemctl start apache2
sudo chown -R $USER:$USER /var/www/html
sudo echo "<html><body><h1> HELLO THIS IS MODULE-1 at instance id"
EOF

}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCDE0+QXXtlIFOrtVBs6ogkrjFcUlCNKTjAwsCQGbyyPzzDF6SsCaSaSQhUmxD5K9/phciHyJgTAIWCJlZ5tXJtrtw8BWznONF+SNOWk5GQ9eQkacdKPZ7T6g54jA+PIepyjisSYCbiDKdiboTZeTbZI8N+kvVPcQzjxcNn37MutySQm8BJl4AZgsobuJosDEj5Ow3TVHmhaHzREqdbkML5lSUEEOAcGhkvk2nA5vIl34diiCJf33nv+i6Nm8PzK/1SdRIU3wUeniFDf22w/u2Wk9hNKYxNnyA/dgdk7Zr0WYtbvQfCuyTXZo/iCkzzsELEnzs5cy+x/3IhE6fTqcmd Annalect"
}

resource "aws_security_group" "my_sg" {
  name        = "my_sg"
  description = "Allow ssh access"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
