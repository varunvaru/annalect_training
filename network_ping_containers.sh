#!/bin/bash

echo "creating 5 containers"
for i in {1..5}
do
docker build -t container$i projects/

done
echo -n "Please enter the network name: "
read network_name

echo "Creating a customised bridge network"
docker network create --driver bridge $network_name

echo "Connecting each container to the network '$network_name'..."
for i in {1..5}
do
   # docker network connect $network_name container$i
    docker run -dt --name con$i --network $network_name container$i bash
done


docker ps
read -p "enter the container name to ping" name
echo "Performing a ping test..."
docker exec -it con1  ping -c 4 $name 
echo "Ping test completed!"
