#!/bin/bash


echo "Welcome to the Sign Up/Sign In"

function sign_up {
	#Sign up a new user
	echo "Please enter a username:"
	read username
	echo "Please enter a password with atleast one number and a symbol:"
	read -s password
	echo " please re-enter the password:"
	useradd -m -s /bin/bash -p $(openssl passwd -1 $password) $username
}

function sign_in {
	#Sign in an existing user
	echo "Please enter your username:"
	read username
	echo "Please enter your password:"
	read -s password
	echo "Welcome back, $username"
	su "$username"
}

echo "Would you like to Sign Up (1) or Sign In (2)?"
read choice

case $choice in
	1)
	sign_up
	;;
	2)
	sign_in
	;;
	*)
	exit
	;;
esac

sh taking_test.sh
sh test_file.sh
