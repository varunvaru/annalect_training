#!/bin/bash

REGION="ap-south-1"

INSTANCES=$(aws ec2 describe-instances --region $REGION --query 'Reservations[].Instances[].[InstanceId,InstanceType,State.Name,PublicIpAddress,PrivateIpAddress]' --output text)

#  header row for the CSV file
HEADER="Instance ID,Instance Type,State,Public IP,Private IP"

echo $HEADER > ec2-inventory.csv
echo "$INSTANCES" >> ec2-inventory.csv

echo "EC2 instance inventory has been created and saved to ec2-inventory.csv."

