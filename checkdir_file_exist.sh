#!/bin/bash

echo -e "Please enter the directory path: "
read dir_path

if [ -n "$dir_path" ]
then
    if [ -d $dir_path ]
    then
        for file in $dir_path/*
        do
            if  [[ $file == "xyz.pdf" ]]
            then
                cat $file
            fi
        done
    else
        echo "Directory not found."
    fi
else
    echo "Input variable is blank or null."
fi
