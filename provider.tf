provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "web" {
  ami           = "ami-0cca134ec43cf708f"
  instance_type = var.ec2_instance_type

  subnet_id = "subnet-0250247c952a46b9e"
}
