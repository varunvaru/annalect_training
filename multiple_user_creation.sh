#!/bin/bash

# Ask the user for a username
read -p "Please enter a username: " username

# Ask the user for a password
read -s -p "Please enter the password for $username: " password

# Create the user
useradd -m -s /bin/bash -p $(openssl passwd -1 $password) $username

# Ask the user to input another username
read -p "Do you want to create another user (y/n)? " answer

# Loop until the user doesn't want to create another user
while [ "$answer" == "y" ]
do
    read -p "Please enter a username: " username
    read -s -p "Please enter the password for $username: " password
    useradd -m -s /bin/bash -p $(openssl passwd -1 $password) $username
    read -p "Do you want to create another user (y/n)? " answer
done

echo "Done creating users!"
